#!/usr/bin/perl
use DBD::mysql;
use Getopt::Std;
use lib "/usr/share/perl/5.10.1";
sub parse_and_create
{
my ($input_string, $r_flag)=@_;
	if ($input_string=~/(\w{2}\:\w{2}\:\w{2}\:\w{2}\:\w{2}\:\w{2})\s(\d{1,3})/i)
		{
			$stb_mac=$1;
			$chan_name=$2;
			if ($r_flag=='0')
			{
				$backup_current_channel=$dbh->do("UPDATE stalker_db.last_id SET last_id_bak=last_id WHERE ident='$stb_mac'");
				$revert_chan_event=$dbh->do("INSERT INTO stalker_db.events VALUES (NULL, (SELECT id from users where mac='$stb_mac'), '', 'play_channel', '$chan_name', 0, 0, 0, 0, 0, 0, 2, NOW(), NOW() + interval 1 hour, 0)");	
			}
				else{
					$create_ch_channel_event=$dbh->do("INSERT INTO stalker_db.events VALUES (NULL, (SELECT id from users where mac='$stb_mac'), '', 'play_channel', (SELECT last_id_bak FROM stalker_db.last_id WHERE ident=$stb_mac, 0, 0, 0, 0, 0, 0, 2, NOW(), NOW() + interval 1 hour, 0)");
				}
		}else {print "Validation failed:\nString: $input_string";}

}


my $user="root";
my $passwd="sport-life2008";
my $mysql_host="172.28.1.252";
my $mysql_db="stalker_db";
$dbh = DBI->connect("DBI:mysql:database=$mysql_db;host=$mysql_host", $user, $passwd,{mysql_enable_utf8 => 1}) || die "Could not connect to database: $DBI::errstr";

getopt ('f:i:c:r',\%opts);

if (!($opts{'f'}|$opts {'i'}))
{
	print "
Usage: 
$0 -f /path/to/list.txt
$0 -i <mac> -c <channel id>

Reverse mode:
script.pl -f /path/to/list.txt -r 1
script.pl -i <mac> -r 1
";
exit 0;
}

if ($opts{'r'}){my $r_flag='1';print "Reverse mode";$opts{'c'}='0';}else{my $r_flag='0';}
if ($opts{'f'})
	{
		open F, $opts{'f'};
		my @chan_change_list=<F>;
		close F;
		
		foreach my $entry (@chan_change_list)
			{
				parse_and_create($entry, $r_flag);
			}
	}else {
		parse_and_create ("$opts{'i'} $opts{'c'}",$r_flag);
	}

