#!/usr/bin/expect -f
set timeout 2
set USER "root"
set PASS "930920"
set SUBNET "172.28.246"
set PORTAL "http://stalker.corp.sportlife.ua"

foreach HOST {22 25} {
  spawn ssh $USER@$SUBNET.$HOST
    expect "(yes/no)?*"
    send "yes\r"
    expect "*word:"
    sleep 1
    send "$PASS\n"
    expect "#*"
    send "killall stbapp\r"
    expect "#*"
    send "fw_setenv portal1 $PORTAL\r"
    expect "#*"
    send "reboot\r"
    expect eof
}
