#!/bin/bash
function GET_CONN_OPTS
{
	# Читаем настройки подключения к API/DB из custom.ini.
	# На случай, если Middleware настраивал идиот - смотрим и в server.ini

		for OPTION in api_auth_login api_auth_password mysql_host mysql_user mysql_pass db_name
		do
			# custom.ini
			# Ищем строку, вынимаем значение параметра, убираем лишнее
			RESULT=$(cat $STALKER_DIR/server/custom.ini | grep $OPTION | egrep -v ";|#" | sed -s "s/.*=\s//" | sed -s "s/\r//")

			
			# Если все-таки произошла пичалька и не получилось - смотрим в config.ini
			if [[ -z "$RESULT" ]]
			then
				RESULT=$(cat $STALKER_DIR/server/config.ini | grep $OPTION | egrep -v ";|#" | sed -s "s/.*=\s//" | sed -s "s/\r//")
				
				# Если и тут ничего нет - завершаем работу скрипта
				if [[ -z "$RESULT" ]]
				then
					echo "Value  $OPTION not found. STOP"
					exit 1
				fi
			fi

			# Если все-таки есть - сохраняем настройки
			OPTS["$OPTION"]=$RESULT
		done

		# DB init
		MYSQL_BIN=$(which mysql)
		DB_CONN="$MYSQL_BIN -sn -u ${OPTS["mysql_user"]} -p${OPTS["mysql_pass"]} -h ${OPTS["mysql_host"]} ${OPTS["db_name"]}"
		if [[ -z "$MYSQL_BIN" ]]
        	then
        		echo "Die: mysql client binary not found"
        		exit 1
        	fi
		
		# DB customization check
#		DB_STATUS_TABLE_QUERY="SHOW TABLES LIKE 'users_channels'";
#		
#		if [[ -z "$(echo "$DB_STATUS_QUERY" | $DB_CONN)" ]]
#		then
#			echo "Die: database is not prepared"
#			exit 1
#		fi 

	
}

function LOAD_STATIC_BIND
{
	# Загружаем статические привязки (файл формата 
	# key = value
	while read line || [[ -n $line ]]; do
	[[ "$line" =~ ^#|^! ]] && continue;
	if [[ "${line% =*}" ]]; then STATIC[${line% =*}]="${line#*= }" ; fi ;
	done < $1 ; 
}


function GET_CURR_CHAN_NUM
(
	# Получаем из БД номер канала, который в настоящий момент запущен на приставке
	# Приставку идентифицируем по MAC-адресу
	
	MAC=$1
	
	if [[ -z "$MAC" ]]
	then
		echo "Die: no STB Mac provided"
		exit 1
	fi
	
	DB_LCHAN_QUERY="SELECT number FROM itv WHERE name=(SELECT now_playing_content FROM users WHERE mac='$MAC')"
	
	# Выполняем запрос к БД и возвращаем номер канала 
	echo "$DB_LCHAN_QUERY" | $DB_CONN
)

function GET_STB_STATE
{
	# Получаем из БД текущее состояние  приставки 1 - offline, 0 - online
	MAC=$1

        if [[ -z "$MAC" ]]
        then
                echo "Die: no STB Mac provided"
                exit 1
        fi

	DB_GET_STB_STATE_QUERY="SELECT COUNT(*) FROM users WHERE keep_alive < (NOW()-'90 sec') AND mac='$MAC'"
	echo "$DB_GET_STB_STATE_QUERY" | $DB_CONN

}
function GET_STB_LOGIN
{
	# Получаем из БД логин (описание) приставки. Нужно для принудительной коррекции канала в некоторых случаях
	MAC=$1

        if [[ -z "$MAC" ]]
        then
                echo "Die: no STB Mac provided"
                exit 1
        fi

        MYSQL_BIN=$(which mysql)

        if [[ -z "$MYSQL_BIN" ]]
        then
        	echo "Die: mysql client binary not found"
        	exit 1
        fi

        if [[ -z "$DB_CONN" ]]
        then
                echo "Die: Database connection options are not defined"
                exit 1
        fi

        DB_LOGIN_QUERY="SELECT login FROM users WHERE mac='$MAC'"

        # Выполняем запрос к БД и возвращаем логин
        echo "$DB_LOGIN_QUERY" | $DB_CONN
}

function GET_STB_EXISTS
{
	# Проверяем, существует ли приставка
        MAC=$1

        if [[ -z "$MAC" ]]
        then
                echo "Die: no STB Mac provided"
                exit 1
        fi

        DB_GET_STB_EXISTS_QUERY="SELECT COUNT(*) FROM users WHERE mac='$MAC'"
        echo "$DB_GET_STB_EXISTS_QUERY" | $DB_CONN

}
function GET_CHAN_NUM
{
	# Получаем из БД номер канала по названию
	IPTV_CHAN_NAME=$1
	
	if [[ -z "$IPTV_CHAN_NAME" ]]
	then
		echo "Die: no iptv chan name provided"
		exit 1
	fi

	DB_GET_CHAN_NUM_QUERY="SELECT number FROM itv WHERE name='$IPTV_CHAN_NAME'"
	echo "$DB_GET_CHAN_NUM_QUERY" | $DB_CONN
	
}
function PLAY_CHANNEL
{
	# Переключаемся на рекламу, ждем пока закончится ролик, переключаемся обратно
	# Размер паузы задается в качестве параметра скрипту
	# MAC-адрес приставки
	C_MAC=$1
	
	# Номер канала для обратного переключения
	C_CHAN_NUM=$2

	
	# Определяем пути к CURL
	CURL_BIN=$(which curl)

	if [[ -z "$CURL_BIN" ]]
	then
		echo "Die: curl binary not found"
		exit 1
	fi
	
	# Собираем параметры и адрес запроса к STALKER HTTP API в кучку
	POST_DATA_ADV="event=play_channel&channel=$ADV_CHAN_NUM"
	POST_DATA_ORIG="event=play_channel&channel=$C_CHAN_NUM"
	URL="http://${OPTS["api_auth_login"]}:${OPTS["api_auth_password"]}@localhost/stalker_portal/api/send_event/$C_MAC"

	# Сохраняем текущий канал в отдельной таблице (кривое API STALKER, workaround)
	#DB_USERS_CHANS_UPDATE_QUERY="REPLACE INTO users_channels SET mac='$C_MAC' now_playing content='$C_CHAN_NUM'"
	#echo "$DB_USERS_CHANS_UPDATE_QUERY" | $DB_CONN
	
  	# Запускаем в фоне отдельным процессом задачу переключения
	# Шаманство после "sleep" используется в качестве workaround'a при длине ролика менее 1 минуты
	$(
		$CURL_BIN --data "$POST_DATA_ADV" "$URL"
		sleep "$MOVIE_LENGTH""s"

		REAL_CHAN_NUM=""
		
		for TRY_PLAY in `seq 1 99`
		do
			REAL_CHAN_NUM=$(GET_CURR_CHAN_NUM "$C_MAC")
			if [[ "$REAL_CHAN_NUM" != "$C_CHAN_NUM" ]]
			then 
				$CURL_BIN --data "$POST_DATA_ORIG" "$URL"
				sleep 20s
			else
				break
			fi
		done
	) &

}
function PROC_STB_LIST
{
	# Проверяем наличие файла со списком STB
	if [[ ! -f "$STB_LIST_FILE" ]]
	then
		echo "STB list not found"
		exit 1
	fi

	# Открываем и проходимся по каждой приставке
	for STB_MAC in `cat $STB_LIST_FILE`
	do
		# Проверяем формат MAC-адреса. Кривые строки игнорируем
		MAC_CHECK=$(echo $STB_MAC | grep -P "\w{2}\:\w{2}\:\w{2}\:\w{2}\:\w{2}\:\w{2}")
		if [[ -z "$MAC_CHECK" ]]
		then
			echo "String $STB_MAC has incorrect format, skip"
			continue
		fi

		# Проверяем наличие приставки в Middleware
		EXISTS_CHECK=$(GET_STB_EXISTS "$STB_MAC")

		if [[ "$EXISTS_CHECK" == "0" ]]
                then
                        echo "STB $STB_MAC does not exist in middleware, skip"
                        continue
                fi

		# Проверяем состояние приставки. Если приставка в оффлайне (1)- пропускаем
		STATE_CHECK=$(GET_STB_STATE "$STB_MAC")

		if [[ "$STATE_CHECK" == "1" ]]
		then
			echo "STB $STB_MAC is offline, skip"
			continue
		fi		

		# Проверяем наличие статической привязки приставка <> канал
		# Если привязки нет - в качестве канала для обратного переключения используется то, что приставка воспроизводит *сейчас*
		if [[ "${STATIC[$STB_MAC]}" ]]
          	 	then CURR_CHAN_NUM=${STATIC[$STB_MAC]}
		 	else CURR_CHAN_NUM=$(GET_CURR_CHAN_NUM "$STB_MAC")
		fi

		if [[ -z "$CURR_CHAN_NUM" || "$CURR_CHAN_NUM" -ge "990" ]]
		then
			# Ненаучное шаманство для обхода проблемы с "потерей" значения поля now_playing_content
			# Если применяется этот алгоритм (принудительная установка приставки на канал по-умолчанию)- что-то очень сильно пошло не так
			# Шаманство "на сейчас" - приставки на рецепциях и кассах при потере значения переключаются на SportLife.TV
			# Все остальные - на Eurosport

			echo "Current content for $STB_MAC not found, skip"
			STB_LOGIN=$(GET_STB_LOGIN "$STB_MAC")

			if [[ ! -z "$(echo \"$STB_LOGIN\" | grep -Pi 'rec|ress|res|kassa|cassa|cashdesk')" ]]
			then
				CURR_CHAN_NUM=$(GET_CHAN_NUM "SportLife.TV")
			else
				CURR_CHAN_NUM=$(GET_CHAN_NUM "Eurosport")
			fi

		fi

		# Переключаемся на рекламу и обратно
		PLAY_CHANNEL "$STB_MAC" "$CURR_CHAN_NUM"
	done
}


# Читаем параметры, с которыми запущен скрипт
# Список маков
STB_LIST_FILE=$1

# Номер канала с рекламой
ADV_CHAN_NUM=$2

# Длина рекламного ролика (в секундах)
MOVIE_LENGTH=$3

# Проверяем наличие всех обязательных параметров
if [[ -z "$STB_LIST_FILE" || -z "$ADV_CHAN_NUM" || -z "$MOVIE_LENGTH" ]]
then
	echo "Usage: $0 <stb list text file> <channel number> <movie length in seconds>"
    exit 1
fi

# Определяем переменные (если надо)
STALKER_DIR="/var/www/stalker_portal"
STATIC_BIND_F="/tv-schedule/.static"
declare -A OPTS
declare -A STATIC

# Проверяем наличие файлов с данными
# Папка с настройками Stalker Middleware
if [[ ! -d "$STALKER_DIR" ]]
then
	echo "Die: stalker dir not found"
    	exit 1
fi

if [[ ! -f "$STB_LIST_FILE" ]]
then
	echo "Die: STB list file not found"
	exit 1
fi

# Загружаем файл со статическими привязками
# Если не существует - создаем пустой
if [[ ! -f "$STATIC_BIND_F" ]]
then
	touch $STATIC_BIND_F
else
	LOAD_STATIC_BIND $STATIC_BIND_F
fi

# Считываем настройки
GET_CONN_OPTS

# Открываем список и последовательно проходимся по всем STB
PROC_STB_LIST
